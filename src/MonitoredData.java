import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;

public class MonitoredData {
    private String startTime;
    private String endTime;
    private String activityLabel;

    public MonitoredData(String startTime, String endTime, String activityLabel) {
        super();
        this.startTime = startTime;
        this.endTime = endTime;
        this.activityLabel = activityLabel;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getActivityLabel() {
        return activityLabel;
    }

    public void setActivityLabel(String activityLabel) {
        this.activityLabel = activityLabel;
    }

    @Override
    public String toString() {
        return new String("start time: " + startTime.toString() + ", end time: " + endTime.toString() + ", type: "
                + activityLabel.toString() + "\n");
    }

    public String returnDay() {
        return startTime.substring(8, 10).toString();
    }

    public int returnFirstDayInt() {
        @SuppressWarnings("unused")
        Integer i;
        return i = Integer.valueOf(startTime.substring(8, 10).toString());
    }

    public int returnSecondDayInt() {
        @SuppressWarnings("unused")
        Integer i;
        return i = Integer.valueOf(endTime.substring(8, 10).toString());
    }

    // Re-think this approach
    public int durationHours() {
        DateTime dt1, dt2;
        org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        dt1 = formatter.parseDateTime(startTime);
        dt2 = formatter.parseDateTime(endTime);

        Period p = new Period(dt1, dt2);
        int hours = p.getHours();

        return hours;
    }

    public int durationMinutes() {
        DateTime dt1, dt2;
        org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        dt1 = formatter.parseDateTime(startTime);
        dt2 = formatter.parseDateTime(endTime);

        Period p = new Period(dt1, dt2);
        int minutes = p.getMinutes();

        return minutes;
    }
}
