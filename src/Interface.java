import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class Interface extends JFrame {

    private JPanel buttons;
    private JPanel result;
    private JPanel finalPanel;

    private JButton distinctDays = new JButton("Distinct Days");
    private JButton actionNumber = new JButton("Number of occurances of activity in the log");
    private JButton dayActivies = new JButton("Activites per day");
    private JButton totalDuration = new JButton("Total duration of activities");
    private JButton quickActivities = new JButton("Activities with duration less than 5 minutes");

    public JTextField numberOfDistinctDays = new JTextField("", 2);

    Interface() {
        buttons = new JPanel();
        buttons.setLayout(new BoxLayout(buttons, BoxLayout.Y_AXIS));

        distinctDays.setAlignmentX(Component.CENTER_ALIGNMENT);
        actionNumber.setAlignmentX(Component.CENTER_ALIGNMENT);
        dayActivies.setAlignmentX(Component.CENTER_ALIGNMENT);
        totalDuration.setAlignmentX(Component.CENTER_ALIGNMENT);
        quickActivities.setAlignmentX(Component.CENTER_ALIGNMENT);

        buttons.add(Box.createRigidArea(new Dimension(15, 15)));
        buttons.add(actionNumber);
        buttons.add(Box.createRigidArea(new Dimension(15, 15)));
        buttons.add(dayActivies);
        buttons.add(Box.createRigidArea(new Dimension(15, 15)));
        buttons.add(totalDuration);
        buttons.add(Box.createRigidArea(new Dimension(15, 15)));
        buttons.add(quickActivities);
        buttons.add(Box.createRigidArea(new Dimension(15, 15)));

        result = new JPanel();
        result.setLayout(new FlowLayout());
        result.add(distinctDays);
        result.add(numberOfDistinctDays);

        finalPanel = new JPanel();
        finalPanel.setLayout(new BoxLayout(finalPanel, BoxLayout.Y_AXIS));
        finalPanel.add(buttons);
        finalPanel.add(result);

        this.setContentPane(finalPanel);
        this.setTitle("Stream Processing using Lambda Expressions");
        this.pack();
        setSize(500, 265);
        setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void setDistinctDaysButtonListener(ActionListener a) {
        distinctDays.addActionListener(a);
    }

    public void setActivityNumberButtonListener(ActionListener a) {
        actionNumber.addActionListener(a);
    }

    public void setDayActiviesButtonListener(ActionListener a) {
        dayActivies.addActionListener(a);
    }

    public void setTotalDurationButtonListener(ActionListener a) {
        totalDuration.addActionListener(a);
    }

    public void setQuickActivitiesButtonListener(ActionListener a) {
        quickActivities.addActionListener(a);
    }

}
