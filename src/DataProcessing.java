import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DataProcessing {

    private Interface gui;
    private String fileName = "D://OneDrive/Coding/IdeaProjects/Programming_Techniques_Assignment_5/src/Activities.txt";
    private List<MonitoredData> myList = new ArrayList<>();

    DataProcessing() {
        gui = new Interface();
        gui.setDistinctDaysButtonListener(new DistinctDayButtonListener());
        gui.setActivityNumberButtonListener(new ActivityNumberButtonListener());
        gui.setDayActiviesButtonListener(new DayActiviesButtonListener());
        gui.setTotalDurationButtonListener(new TotalDurationButtonListener());
        gui.setQuickActivitiesButtonListener(new QuickActivitiesButtonListener());
        try {
            myList = Files.lines(Paths.get(fileName)).map(mD -> mD.split("\t\t"))
                    .map(a -> new MonitoredData(a[0], a[1], a[2])).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class DistinctDayButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent arg0) {
            List<String> dateString = myList.stream().map(MonitoredData::returnDay).collect(Collectors.toList());
            long nr = dateString.stream().distinct().count();
            gui.numberOfDistinctDays.setText(String.valueOf(nr));
        }
    }

    private class ActivityNumberButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent arg0) {
            Map<String, Long> activityOccurence = myList.stream()
                    .collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));

            activityOccurence.entrySet().forEach(entry -> {
                try {
                    PrintWriter writer = new PrintWriter(new FileWriter("Activity Occurence.txt"));
                    writer.println(entry.getKey() + " - " + entry.getValue());
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            System.out.println("Task 2 - The information has been written into the file!");

            try {
                Desktop.getDesktop().open(new java.io.File("Activity Occurence.txt"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class DayActiviesButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent arg0) {
            Map<Integer, Map<String, Long>> activityOccurenceForDay = myList.stream()
                    .collect(Collectors.groupingBy(MonitoredData::returnFirstDayInt,
                            Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting())));

            // Pretty print for this data structure
            try {
                PrintWriter writer = new PrintWriter(new FileWriter("Day Activities.txt", true));
                Iterator<Map.Entry<Integer, Map<String, Long>>> parent = activityOccurenceForDay.entrySet().iterator();
                while (parent.hasNext()) {
                    Map.Entry<Integer, Map<String, Long>> parentPair = parent.next();
                    writer.println(parentPair.getKey());

                    Iterator<Map.Entry<String, Long>> child = (parentPair.getValue()).entrySet().iterator();
                    while (child.hasNext()) {
                        @SuppressWarnings("rawtypes")
                        Map.Entry childPair = child.next();
                        writer.println(childPair.getKey() + " = " + childPair.getValue());

                        child.remove(); // avoids a
                        // ConcurrentModificationException
                    }
                    writer.println();
                }
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Task 3 - The information has been written into the file!");

            try {
                Desktop.getDesktop().open(new java.io.File("Day Activities.txt"));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private class TotalDurationButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent arg0) {
            // System.out.println(myList.toString());
            System.out.println("Task 4 - The information has been written into the file!");
            // Do a merge sort type of implementation
        }
    }

    private class QuickActivitiesButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent arg0) {
            // List of all activities
            Map<String, Long> activityOccurence = myList.stream()
                    .collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));

            // List of activities with counter of less than 5 minutes
            Map<String, Long> minDuration = myList.stream().filter(min -> min.durationMinutes() < 5)
                    .collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));

            // Final list of requested activities
            List<String> finalList = new ArrayList<>();
            finalList = activityOccurence.entrySet().stream().filter(key -> minDuration.get(key.getKey()) != null)
                    .filter(a -> 0.9 * a.getValue() <= minDuration.get(a.getKey())).map(k -> k.getKey())
                    .collect(Collectors.toList());
            System.out.println();

            finalList.forEach(entry -> {
                try {
                    PrintWriter writer = new PrintWriter(new FileWriter("Request Five.txt", true));
                    writer.println(entry.toString());
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });

            System.out.println("Task 2 - The information has been written into the file!");

            try {
                Desktop.getDesktop().open(new java.io.File("Request Five.txt"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}